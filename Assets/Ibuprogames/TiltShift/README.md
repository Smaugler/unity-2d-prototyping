# Tilt Shift

'Tilt Shift', or Diorama Effect, it's an effect commonly used in photography whereby objects look like miniature scale models.

## Getting Started

After install, read the documentation at folder Ibuprogames/TiltShift/Documentation or visit the [online version](http://www.ibuprogames.com/2019/10/02/tilt-shift/).

## Requisites

Unity 2017, 2018 or 2019.

## Running the demo

Open the scene at folder Ibuprogames/TiltShift/Demo/Scene. You can delete the forder if you want.

## Bugs and/or questions

Please send us an email to hello@ibuprogames.com.

## Authors

* **Martin Bustos** - [@Nephasto](https://twitter.com/Nephasto)

## License

See the [LICENSE.md](LICENSE.md) file for details
