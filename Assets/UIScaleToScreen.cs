﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIScaleToScreen : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Camera Main_Camera = GameObject.Find("Main Camera").GetComponent<Camera>();
        
        RectTransform Item = GetComponent<RectTransform>();
        Item.localScale = new Vector3(Main_Camera.aspect, Main_Camera.aspect, Main_Camera.aspect);

        // If The gameobject is the tutorial box shift it down
        if (transform.name == "Tutorial_Text_Background")
        {
            RectTransform ScoreRect = GameObject.Find("Score_Text_Background").GetComponent<RectTransform>();
            Item.position = new Vector3(Item.position.x, Item.position.y - (ScoreRect.sizeDelta.y/2), Item.position.z);
        }

        if(transform.name == "Highscore_Text_Background")
        {
            Item.position = new Vector3(Screen.width-Item.sizeDelta.x * Main_Camera.aspect, Item.position.y, Item.position.z);
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
