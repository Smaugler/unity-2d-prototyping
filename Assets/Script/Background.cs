﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background : MonoBehaviour
{
    private GameObject Player;
    private Camera Main_Camera;

    // Start is called before the first frame update
    void Start()
    {
        Player = GameObject.Find("CharacterRobotBoy");
        Main_Camera = GameObject.Find("Main Camera").GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        
        if (Player.transform.position.y > Main_Camera.transform.position.y && transform.position.y > Main_Camera.transform.position.y)
        {
            transform.position = Vector3.Lerp(new Vector3(transform.position.x, transform.position.y, transform.position.z), new Vector3(transform.position.x, transform.position.y - 1, transform.position.z), Time.deltaTime * 1.0f);
        }

        if (Player.transform.position.y < Main_Camera.transform.position.y - 3.75f && transform.position.y <= (Main_Camera.transform.position.y + 2.5f))
        {
            transform.position = Vector3.Lerp(new Vector3(transform.position.x, transform.position.y, transform.position.z), new Vector3(transform.position.x, transform.position.y + 1, transform.position.z), Time.deltaTime * 10.0f);
        }

        if (Player.transform.position.y < Main_Camera.transform.position.y && transform.position.y < (Main_Camera.transform.position.y + 2.5f))
        {
            transform.position = Vector3.Lerp(new Vector3(transform.position.x, transform.position.y, transform.position.z), new Vector3(transform.position.x, transform.position.y + 1, transform.position.z), Time.deltaTime * Main_Camera.GetComponent<Camera2D>().Speed);
        }
    }
}
