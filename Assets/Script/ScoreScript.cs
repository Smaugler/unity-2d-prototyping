﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreScript : MonoBehaviour
{

    public static int scoreValue = 0;
    public static int Highscore = 0;
    Text score;
    
    // Start is called before the first frame update
    void Start()
    {
        score = GetComponent<Text>();
        scoreValue = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if(scoreValue >= Highscore)
        {
            Highscore = scoreValue;
        }

        if(transform.name == "Score")
        {
            score.text = "Score: " + scoreValue;
        }

        if(transform.name == "Highscore")
        {
            score.text = "Highscore: " + Highscore;
        }
    }
}
