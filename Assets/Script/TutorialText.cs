﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialText : MonoBehaviour
{
    Text TuturialText;
    private BlockSpawner Spawner;
    private Level_Manager LvlManager;
    private GameObject TextBackground;
    private Restarter Killzone;

    // Start is called before the first frame update
    void Start()
    {
        TextBackground = GameObject.Find("Tutorial_Text_Background");
        TuturialText = GetComponent<Text>();
        LvlManager = GameObject.Find("GameManager").GetComponent<Level_Manager>();
        Spawner = GameObject.Find("Platform Spawner").GetComponent<BlockSpawner>();
        Killzone = GameObject.Find("Killzone").GetComponent<Restarter>();
        TuturialText.text = "";
    }

    // Update is called once per frame
    void Update()
    {
        LvlManager = GameObject.Find("GameManager").GetComponent<Level_Manager>();

        if (Killzone.Death == true && LvlManager.bTutorialComplete == true)
        {
            LvlManager.Jumps = LvlManager.JumpTask;
            LvlManager.WallJumps = LvlManager.WallJumpTask;
            LvlManager.Attacks = LvlManager.AttackTask;
        }

        if(LvlManager.Jumps < LvlManager.JumpTask)
        {
            TuturialText.text = "Use 'A' and 'D'\nto move left and right.\nUse Spacebar to jump.\nTry Jumping\n" + LvlManager.JumpTask + " Times! " + LvlManager.Jumps  + "/" + LvlManager.JumpTask;
        }
        else if (LvlManager.WallJumps < LvlManager.WallJumpTask)
        {
            TuturialText.text = "You Can Walljump\nBy Jumping On Walls!\nTry Walljumping\n" + LvlManager.WallJumpTask + " Times! " + LvlManager.WallJumps + "/" + LvlManager.WallJumpTask;
        }
        else if (LvlManager.Attacks < LvlManager.AttackTask)
        {
            Spawner.NormalEnemySpawnRate = 1;
            TuturialText.text = "You Can Attack\nEnemies By Pressing 'J'!\nTry Hitting\n" + LvlManager.WallJumpTask + " Enemies! " + LvlManager.Attacks + "/" + LvlManager.AttackTask;
        }
        else if (LvlManager.bTutorialComplete == false)
        {
            Spawner.NormalEnemySpawnRate = 0.35f;
            LvlManager.bTutorialComplete = true;
            TextBackground.GetComponent<Image>().color = new Color(0.0f, 0.0f, 0.0f, 0.0f);
            TuturialText.text = "";
        }
    }
}
