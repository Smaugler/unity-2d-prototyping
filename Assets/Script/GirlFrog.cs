﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GirlFrog : MonoBehaviour
{

   
    
    private Animator m_Anim;
    public bool isCheer;

    // Start is called before the first frame update
    void Start()
    {
      m_Anim = GetComponent<Animator>();
      isCheer = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (ScoreScript.scoreValue == 100 || ScoreScript.scoreValue == 2000 || ScoreScript.scoreValue == 3000)
        {
            m_Anim.SetTrigger("kiss");
        }
        if (isCheer)
        {
            m_Anim.SetTrigger("cheer");
            isCheer = false;
        }
    }
}
