﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Camera2D : MonoBehaviour
{
    public float dampTime = 0.15f;
    public Transform Player;
    public float Speed;
    public float CameraLock = 3.0f;
    private Vector3 velocity = Vector3.zero;
    private Camera Main_Camera;
    private GameObject Background;
    private Level_Manager LvlManager;


    // Start is called before the first frame update
    void Start()
    {
        Main_Camera = GameObject.Find("Main Camera").GetComponent<Camera>();
        Player = GameObject.Find("CharacterRobotBoy").GetComponent<Transform>();
        Background = GameObject.Find("Background");
        LvlManager = GameObject.Find("GameManager").GetComponent<Level_Manager>();

        Speed = 0.0f;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.Lerp(new Vector3(transform.position.x, transform.position.y, -5), new Vector3(transform.position.x, transform.position.y + 1, -5), Time.deltaTime * Speed);

        //if (Player.transform.position.y > (Main_Camera.transform.position.y + CameraLock) && Speed < 1.0f && LvlManager.bTutorialComplete == true)
        if (Player.transform.position.y > (Main_Camera.transform.position.y + CameraLock) && Speed < 3.0f)
        {
            Speed += 0.1f;
        }

        if (Input.GetKeyDown("return"))
        {
            SceneManager.LoadScene("Terrain Movement And Death Pit");
        }

    }
}
