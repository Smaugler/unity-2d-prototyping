﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace UnityStandardAssets._2D
{
    public class ToadAI : MonoBehaviour
    {

        //Patrol Variables
        public float speed;
        private float groundDetectionDistance = 1f;
        public bool movingRight;
        public Transform groundDetection;
        public LayerMask whatIsGround;
  
        public LayerMask whatIsWall;

        //Run variables
        public Transform playerDetection;
        public LayerMask whatIsPlayer;
        private float playerDetectionDistance = 10f;
        private bool playerDetected;

        //Attack Variables
        public Transform attackDetection;
        public float attackDetectionRadius = 0.5f;
        private bool isAttacking;
 
        public float EnemyForceAngle;
        public float EnemyForceStrength;
        float lastAttacked = -9999;

        //Action bools
        private bool patrol;
        private bool attack;
        private bool idle;

        //Animation Variables
        public bool isHit = false;
        private Animator myAnimator;

        // Start is called before the first frame update
        void Start()
        {
            movingRight = true;
            myAnimator = GetComponent<Animator>();
        }
        private void OnValidate()
        {

            EnemyForceAngle = Mathf.Clamp(EnemyForceAngle, 0.0f, 90.0f);
            EnemyForceStrength = Mathf.Clamp(EnemyForceStrength, 1.0f, 100.0f);
        }

        private void Awake()
        {
            // Setting up references.
            groundDetection = transform.Find("GroundDetection");
            playerDetection = transform.Find("PlayerDetection");
            attackDetection = transform.Find("AttackDetection");
          
        }
        // Update is called once per frame
        void Update()
        {

            //Raycast line for ground detection
            RaycastHit2D groundCollider = Physics2D.Raycast(groundDetection.position, Vector2.down, groundDetectionDistance, whatIsGround);

            //Raycast line for wall detection
            RaycastHit2D wallCollider = Physics2D.Raycast(groundDetection.position, Vector2.down, groundDetectionDistance, whatIsWall);

            //Raycast line for player detection
            RaycastHit2D playerCollider = Physics2D.Raycast(new Vector2(playerDetection.position.x - 10, playerDetection.position.y), Vector2.right, playerDetectionDistance, whatIsPlayer);

            if (!isHit)
            {
                //Move the enemy
                if (movingRight)
                {
                    transform.Translate(Vector2.right * speed * Time.deltaTime);
                }
                else if (!movingRight)
                {
                    transform.Translate(Vector2.left * speed * Time.deltaTime);
                }

                //Patrol and turn at edges
                if (groundCollider.collider == false || wallCollider.collider == true)
                {
                    if (movingRight == true)
                    {
                        flip();
                    }
                    else
                    {
                        flip();
                    }
                }

                //Player detection animations
                if (playerCollider.collider == true && !playerDetected && !isAttacking)
                {
                    playerDetected = true;
                    speed = 1.5f;
                    myAnimator.SetBool("enemy", playerDetected);
                }
                else if (!playerCollider.collider)
                {
                    playerDetected = false;
                    speed = 0.5f;
                    myAnimator.SetBool("enemy", playerDetected);
                }
            }
            else
            {
                //Death animation trigger
                myAnimator.SetTrigger("death");
            }

            //Attack the Player
            //Attack radious collision
            Collider2D[] enemyKill = Physics2D.OverlapCircleAll(attackDetection.position, attackDetectionRadius, whatIsPlayer);

            for (int i = 0; i < enemyKill.Length; i++)
            {
                if (enemyKill[i].gameObject != gameObject)
                {
                    if (!isAttacking)
                    {
                        lastAttacked = Time.time;
                    }
                      

                    isAttacking = true;
                    myAnimator.SetTrigger("attack");


                    if (isAttacking)
                    {

                        if (Time.time > lastAttacked + 1.0f / 2.0f)
                        {
                            
                            float fAngle = EnemyForceAngle / 180;
                            float fForceDirection = 1.0f;
                            if (enemyKill[i].gameObject.GetComponent<PlatformerCharacter2D>().m_FacingRight)
                            {
                                enemyKill[i].gameObject.GetComponent<PlatformerCharacter2D>().Flip();
                            }

                            if (!movingRight)
                            {
                                fForceDirection = -1.0f;
                                if (!enemyKill[i].gameObject.GetComponent<PlatformerCharacter2D>().m_FacingRight)
                                {
                                    enemyKill[i].gameObject.GetComponent<PlatformerCharacter2D>().Flip();
                                }
                            }

                            enemyKill[i].gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(Mathf.Cos(fAngle * Mathf.PI) * EnemyForceStrength * 100 * fForceDirection, Mathf.Sin(fAngle * Mathf.PI) * EnemyForceStrength * 100));
                            isAttacking = false;

                        }
                        
                    }
                }
                
            }

        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawLine(groundDetection.position, new Vector2(groundDetection.position.x, groundDetection.position.y - groundDetectionDistance));
            Gizmos.color = Color.red;
            Gizmos.DrawLine(new Vector2(playerDetection.position.x - 10, playerDetection.position.y), new Vector2(playerDetection.position.x + playerDetectionDistance, playerDetection.position.y));
            Gizmos.color = Color.black;
            Gizmos.DrawWireSphere(attackDetection.position, attackDetectionRadius);

        }

        public void DeathAnimation()
        {
            isHit = true;

        }

        public void flip()
        {
            movingRight = !movingRight;

            transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);

        }
    }
}