﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterMovement : MonoBehaviour
{


    public float speedY = 0.01f;
   
    private float curY;


    // Start is called before the first frame update
    void Start()
    {
   
        curY = GetComponent<Renderer>().material.mainTextureOffset.y;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        
        curY += Time.deltaTime/2;
        GetComponent<Renderer>().material.SetTextureOffset("_MainTex", new Vector2(0, curY));
    }
}
