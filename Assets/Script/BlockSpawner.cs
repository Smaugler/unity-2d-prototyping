﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockSpawner : MonoBehaviour
{
    public GameObject DisposablePlatform;
    public GameObject Horizontal_Platform;
    public GameObject Vertical_Platform;
    public GameObject Block_Platform;
    public GameObject Player;
    public GameObject Enemy;

    public float NormalEnemySpawnRate;
    public float FlyingEnemySpawnRate;
    public float MaxSpawnDistance = 5;
    public float MinSpawnDistance = 1;

    private Level_Manager LvlManager;

    private SpriteRenderer PlayerSprite;
    private Camera Main_Camera;
    private BoxCollider2D HorizontalPlatformCollider;
    private BoxCollider2D SpawnerCollider2D;
    private Vector3 vPlatform;

    private GameObject LeftWall;
    private GameObject RightWall;

    public GameObject StartPlatform;

    public float PlatformInterval;
    private int iNumPlatforms;

    private Vector2 PrevPosition;

    private void OnValidate()
    {
        MaxSpawnDistance = Mathf.Clamp(MaxSpawnDistance, 2.0f, 6.0f);
        MinSpawnDistance = Mathf.Clamp(MinSpawnDistance, 0.0f, MaxSpawnDistance-1.0f);
        
        PlatformInterval = Mathf.Clamp(PlatformInterval, 2.5f, 10.0f);

        NormalEnemySpawnRate = Mathf.Clamp(NormalEnemySpawnRate, 0.0f, 1.0f);
        FlyingEnemySpawnRate = Mathf.Clamp(NormalEnemySpawnRate, 0.0f, 1.0f);
    }

    // Start is called before the first frame update
    void Start()
    {
        PrevPosition.Set(0, 0);
        Main_Camera = GameObject.Find("Main Camera").GetComponent<Camera>();
        PlayerSprite = Player.GetComponent<SpriteRenderer>();
        SpawnerCollider2D = GetComponent<BoxCollider2D>();
        HorizontalPlatformCollider = Horizontal_Platform.GetComponent<BoxCollider2D>();
        StartPlatform = GameObject.Find("StartPlatform");

        LeftWall = GameObject.Find("LeftWall");
        RightWall = GameObject.Find("RightWall");

        LvlManager = GameObject.Find("GameManager").GetComponent<Level_Manager>();

        //PlatformInterval = (((PlayerBoxCollider2D.size.y + PlayerCircleCollider2D.radius) * 2) + PlayerCircleCollider2D.offset.y) * 2 + Horizontal_Platform.GetComponent<BoxCollider2D>().size.y;
        iNumPlatforms = Mathf.FloorToInt((Main_Camera.orthographicSize * 2 + HorizontalPlatformCollider.size.y + 0.5f) / PlatformInterval) + 2;
        
        transform.position = new Vector3(0, ((iNumPlatforms-1) * PlatformInterval - Main_Camera.orthographicSize + (SpawnerCollider2D.size.y/2) + 1.5f), 0);

        for (int i = 0; i < iNumPlatforms; i++)
        {
            if (i != 0)
            {
                // while distance between previous object spawned and generated coordinates are greater than max or less than min set values
                if (i == 1)
                {
                    vPlatform.Set(Random.Range(gameObject.GetComponent<Collider2D>().bounds.min.x + (HorizontalPlatformCollider.size.x / 2), gameObject.GetComponent<Collider2D>().bounds.max.x - (HorizontalPlatformCollider.size.x / 2)), ((i) * PlatformInterval - Main_Camera.orthographicSize + (SpawnerCollider2D.size.y/2) + 1.5f), 0);

                    while (Mathf.Abs(vPlatform.x - StartPlatform.transform.position.x) > MaxSpawnDistance || Mathf.Abs(vPlatform.x - StartPlatform.transform.position.x) < MinSpawnDistance)
                    {
                        vPlatform.Set(Random.Range(gameObject.GetComponent<Collider2D>().bounds.min.x + (HorizontalPlatformCollider.size.x / 2), gameObject.GetComponent<Collider2D>().bounds.max.x - (HorizontalPlatformCollider.size.x / 2)), ((i) * PlatformInterval - Main_Camera.orthographicSize + (SpawnerCollider2D.size.y/2) + 1.5f), 0);
                    }
                }
                else
                {
                    vPlatform.Set(Random.Range(gameObject.GetComponent<Collider2D>().bounds.min.x + (HorizontalPlatformCollider.size.x / 2), gameObject.GetComponent<Collider2D>().bounds.max.x - (HorizontalPlatformCollider.size.x / 2)), ((i) * PlatformInterval - Main_Camera.orthographicSize + (SpawnerCollider2D.size.y / 2) + 1.5f), 0);

                    while (Mathf.Abs(vPlatform.x - PrevPosition.x) > MaxSpawnDistance || Mathf.Abs(vPlatform.x - PrevPosition.x) < MinSpawnDistance)
                    {
                        vPlatform.Set(Random.Range(gameObject.GetComponent<Collider2D>().bounds.min.x + (HorizontalPlatformCollider.size.x / 2), gameObject.GetComponent<Collider2D>().bounds.max.x - (HorizontalPlatformCollider.size.x / 2)), ((i) * PlatformInterval - Main_Camera.orthographicSize + (SpawnerCollider2D.size.y/2) + 1.5f), 0);
                    }
                }

                Setup(i);
            }
        }
    }

    void Setup(int iTrack)
    {
        int iPlatform = Random.Range(1, 3);
        
        if (iPlatform == 1)
        {
            Instantiate(Horizontal_Platform, vPlatform, Quaternion.identity);

            if (((vPlatform.x - (Horizontal_Platform.GetComponent<BoxCollider2D>().size.x / 2)) - (LeftWall.transform.position.x + (LeftWall.GetComponent<BoxCollider2D>().size.x / 2))) > (DisposablePlatform.GetComponent<BoxCollider2D>().size.x * 3 - 2))
            {
                Instantiate(DisposablePlatform, new Vector3(Random.Range((LeftWall.transform.position.x + (LeftWall.GetComponent<BoxCollider2D>().size.x / 2) + (DisposablePlatform.GetComponent<BoxCollider2D>().size.x / 2)), (vPlatform.x - (Horizontal_Platform.GetComponent<BoxCollider2D>().size.x / 2) - (DisposablePlatform.GetComponent<BoxCollider2D>().size.x / 2) - 2)), vPlatform.y, vPlatform.z), Quaternion.identity);
            }
            else if (((RightWall.transform.position.x - (RightWall.GetComponent<BoxCollider2D>().size.x / 2)) - (vPlatform.x + (Horizontal_Platform.GetComponent<BoxCollider2D>().size.x / 2))) > (DisposablePlatform.GetComponent<BoxCollider2D>().size.x * 3 - 2))
            {
                Instantiate(DisposablePlatform, new Vector3(Random.Range((vPlatform.x + (Horizontal_Platform.GetComponent<BoxCollider2D>().size.x / 2) + (DisposablePlatform.GetComponent<BoxCollider2D>().size.x / 2) + 2), (RightWall.transform.position.x - (RightWall.GetComponent<BoxCollider2D>().size.x / 2) - (DisposablePlatform.GetComponent<BoxCollider2D>().size.x / 2))), vPlatform.y, vPlatform.z), Quaternion.identity);
            }
        }
        else if (iPlatform == 2)
        {
            Instantiate(Block_Platform, vPlatform, Quaternion.identity);

            if (((vPlatform.x - (Block_Platform.GetComponent<BoxCollider2D>().size.x / 2)) - (LeftWall.transform.position.x + (LeftWall.GetComponent<BoxCollider2D>().size.x / 2))) > (DisposablePlatform.GetComponent<BoxCollider2D>().size.x * 3 - 2))
            {
                Instantiate(DisposablePlatform, new Vector3(Random.Range((LeftWall.transform.position.x + (LeftWall.GetComponent<BoxCollider2D>().size.x/2) + (DisposablePlatform.GetComponent<BoxCollider2D>().size.x/2)), (vPlatform.x - (Block_Platform.GetComponent<BoxCollider2D>().size.x / 2) - (DisposablePlatform.GetComponent<BoxCollider2D>().size.x / 2) - 2)), vPlatform.y, vPlatform.z), Quaternion.identity);
            }
            else if (((RightWall.transform.position.x - (RightWall.GetComponent<BoxCollider2D>().size.x / 2)) - (vPlatform.x + (Block_Platform.GetComponent<BoxCollider2D>().size.x / 2))) > (DisposablePlatform.GetComponent<BoxCollider2D>().size.x * 3 - 2))
            {
                Instantiate(DisposablePlatform, new Vector3(Random.Range((vPlatform.x + (Block_Platform.GetComponent<BoxCollider2D>().size.x / 2) + (DisposablePlatform.GetComponent<BoxCollider2D>().size.x / 2) + 2), (RightWall.transform.position.x - (RightWall.GetComponent<BoxCollider2D>().size.x / 2) - (DisposablePlatform.GetComponent<BoxCollider2D>().size.x / 2))), vPlatform.y, vPlatform.z), Quaternion.identity);
            }
        }

        PrevPosition.Set(vPlatform.x, vPlatform.y);
    }

    public void Spawn()
    {
        vPlatform.Set(Random.Range(gameObject.GetComponent<Collider2D>().bounds.min.x + (HorizontalPlatformCollider.size.x / 2), gameObject.GetComponent<Collider2D>().bounds.max.x - (HorizontalPlatformCollider.size.x / 2)), gameObject.GetComponent<Collider2D>().bounds.max.y - (SpawnerCollider2D.size.y / 2), 0);

        while (Mathf.Abs(vPlatform.x - PrevPosition.x) > MaxSpawnDistance || Mathf.Abs(vPlatform.x - PrevPosition.x) < MinSpawnDistance)
        {
            vPlatform.Set(Random.Range(gameObject.GetComponent<Collider2D>().bounds.min.x + (HorizontalPlatformCollider.size.x / 2), gameObject.GetComponent<Collider2D>().bounds.max.x - (HorizontalPlatformCollider.size.x / 2)), gameObject.GetComponent<Collider2D>().bounds.max.y - (SpawnerCollider2D.size.y / 2), 0);
        }

        int iPlatform = Random.Range(1, 3);

        if (iPlatform == 1)
        {
            Instantiate(Horizontal_Platform, vPlatform, Quaternion.identity);

            if (((vPlatform.x - (Horizontal_Platform.GetComponent<BoxCollider2D>().size.x / 2)) - (LeftWall.transform.position.x + (LeftWall.GetComponent<BoxCollider2D>().size.x / 2))) > (DisposablePlatform.GetComponent<BoxCollider2D>().size.x * 2))
            {
                Instantiate(DisposablePlatform, new Vector3(Random.Range((LeftWall.transform.position.x + (LeftWall.GetComponent<BoxCollider2D>().size.x / 2) + (DisposablePlatform.GetComponent<BoxCollider2D>().size.x / 2)), (vPlatform.x - (Horizontal_Platform.GetComponent<BoxCollider2D>().size.x / 2) - (DisposablePlatform.GetComponent<BoxCollider2D>().size.x / 2))), vPlatform.y, vPlatform.z), Quaternion.identity);
            }
            else if (((RightWall.transform.position.x - (RightWall.GetComponent<BoxCollider2D>().size.x / 2)) - (vPlatform.x + (Horizontal_Platform.GetComponent<BoxCollider2D>().size.x / 2))) > (DisposablePlatform.GetComponent<BoxCollider2D>().size.x * 2))
            {
                Instantiate(DisposablePlatform, new Vector3(Random.Range((vPlatform.x + (Horizontal_Platform.GetComponent<BoxCollider2D>().size.x / 2) + (DisposablePlatform.GetComponent<BoxCollider2D>().size.x / 2)), (RightWall.transform.position.x - (RightWall.GetComponent<BoxCollider2D>().size.x / 2) - (DisposablePlatform.GetComponent<BoxCollider2D>().size.x / 2))), vPlatform.y, vPlatform.z), Quaternion.identity);
            }
        }
        else if (iPlatform == 2)
        {
            Instantiate(Block_Platform, vPlatform, Quaternion.identity);

            if (((vPlatform.x - (Block_Platform.GetComponent<BoxCollider2D>().size.x / 2)) - (LeftWall.transform.position.x + (LeftWall.GetComponent<BoxCollider2D>().size.x / 2))) > (DisposablePlatform.GetComponent<BoxCollider2D>().size.x * 2))
            {
                Instantiate(DisposablePlatform, new Vector3(Random.Range((LeftWall.transform.position.x + (LeftWall.GetComponent<BoxCollider2D>().size.x / 2) + (DisposablePlatform.GetComponent<BoxCollider2D>().size.x / 2)), (vPlatform.x - (Block_Platform.GetComponent<BoxCollider2D>().size.x / 2) - (DisposablePlatform.GetComponent<BoxCollider2D>().size.x / 2))), vPlatform.y, vPlatform.z), Quaternion.identity);
            }
            else if (((RightWall.transform.position.x - (RightWall.GetComponent<BoxCollider2D>().size.x / 2)) - (vPlatform.x + (Block_Platform.GetComponent<BoxCollider2D>().size.x / 2))) > (DisposablePlatform.GetComponent<BoxCollider2D>().size.x * 2))
            {
                Instantiate(DisposablePlatform, new Vector3(Random.Range((vPlatform.x + (Block_Platform.GetComponent<BoxCollider2D>().size.x / 2) + (DisposablePlatform.GetComponent<BoxCollider2D>().size.x / 2)), (RightWall.transform.position.x - (RightWall.GetComponent<BoxCollider2D>().size.x / 2) - (DisposablePlatform.GetComponent<BoxCollider2D>().size.x / 2))), vPlatform.y, vPlatform.z), Quaternion.identity);
            }
        }

        float fSpawnChance = Random.Range(1, 101);

            // spawn enemy
        if (fSpawnChance < Mathf.Ceil(NormalEnemySpawnRate * 100) && LvlManager.WallJumps >= LvlManager.WallJumpTask)
        {
            Instantiate(Enemy, new Vector3(vPlatform.x, vPlatform.y + 1, vPlatform.z), Quaternion.identity);
            LvlManager.NewEnemy();
        }

        PrevPosition.Set(vPlatform.x, vPlatform.y);
    }

    Vector2 RandomSpawnPoint(Bounds bounds)
    {
        Vector2 tPoint = new Vector2(
            Random.Range(bounds.min.x, bounds.max.x),
            bounds.max.y);

        return tPoint;
    }

    // Update is called once per frame
    void Update()
    {
        //Setup();

        //Debug.Log(Random.Range(2, 4));
    }
}

/*
Platforms stay within level/screen.
Platforms must never be the same x position than above or below platform
Platforms must be close enough every now and then so player doesn't rely on using grappling mechanic
Platforms must always have enough space between each platform for players to jump.
*/
