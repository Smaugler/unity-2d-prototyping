﻿//
// Bachelor of Software Engineering
// Media Design School
// Auckland
// New Zealand
//
// (c) 2019 Media Design School
//
// File Name	: Flyer Move.cs
// Description	: This script implements the flying enemy. They activate when the player is in range and swoop to knock them back
// Author		: Alexander Jenkins
// Mail			: alexander.jen8470@mediadesign.school.nz
//

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityStandardAssets._2D
{
    public class FlyerMove : MonoBehaviour
    {

        private PlatformerCharacter2D thePlayer;
        private FlyerMove theFlyer;
        public float playerRange = 5;
        public LayerMask playerLayer;
        public bool playerInRange;
        private bool attack;
        public float timer;
        public float delay = 1f;
        public float EnemyForceAngle = -60;
        public float EnemyForceStrength = 3;
        private float count = 0;

        // Start is called before the first frame update
        void Start()
        {
            thePlayer = FindObjectOfType<PlatformerCharacter2D>();
            theFlyer = GetComponent<FlyerMove>();
        }

        // Update is called once per frame
        void Update()
        {
            //gameObject.GetComponent<Rigidbody2D>().gravityScale = 0;
            //gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0,0);
            if (count == 0)
            {
                Collider2D playerInRange = Physics2D.OverlapCircle(transform.position, playerRange, playerLayer);
                

                if (playerInRange)
                {
                    if (timer > delay)
                    {
                        count++;
                        attack = true;
                    }
                    else
                    {
                        timer += Time.deltaTime;

                    }
                }


                if (attack)
                {
                    

                    if (playerInRange.transform.position.x < this.transform.position.x)
                    {
                        float fAngle = EnemyForceAngle / 180;
                        float fForceDirection = -4.0f;

                        gameObject.GetComponent<Rigidbody2D>().gravityScale = -1;
                        gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(Mathf.Cos(fAngle * Mathf.PI) * EnemyForceStrength * 100 * fForceDirection, Mathf.Sin(fAngle * Mathf.PI) * EnemyForceStrength * 100));
                        attack = false;
                        timer = 0;
                    }
                    else
                    {
                        float fAngle = EnemyForceAngle / 180;
                        float fForceDirection = 4.0f;

                        gameObject.GetComponent<Rigidbody2D>().gravityScale = -1;
                        gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(Mathf.Cos(fAngle * Mathf.PI) * EnemyForceStrength * 100 * fForceDirection, Mathf.Sin(fAngle * Mathf.PI) * EnemyForceStrength * 100));
                        attack = false;
                        timer = 0;
                    }
                    
                }
            }

            Collider2D playerHit = Physics2D.OverlapCircle(transform.position, 0.5f, playerLayer);

            if (playerHit)
            {
                if (playerHit.transform.position.x < this.transform.position.x)
                {
                    float fAngle = EnemyForceAngle / 180;
                    
                    playerHit.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(Mathf.Cos(fAngle * Mathf.PI)  * -1000, Mathf.Sin(fAngle * Mathf.PI)));
                }
                else
                {
                    float fAngle = EnemyForceAngle / 180;
                   
                    playerHit.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(Mathf.Cos(fAngle * Mathf.PI) * 1000, Mathf.Sin(fAngle * Mathf.PI)));
                }
                }
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, playerRange);

            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(transform.position, 0.5f);
        }
    }
}
