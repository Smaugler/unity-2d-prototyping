﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPlayerConstraint : MonoBehaviour
{
    private Camera Main_Camera;
    private BoxCollider2D m_Collider;
    private float XPosition;

    // Start is called before the first frame update
    void Start()
    {
            // Finds Main Camera Object and Assigns it to public variable
        Main_Camera = GameObject.Find("Main Camera").GetComponent<Camera>();
        m_Collider = GetComponent<BoxCollider2D>();

        XPosition = ((Main_Camera.orthographicSize * Main_Camera.aspect));
        //XPosition = 5;

        // If Wall Is On The Left
        if (transform.position.x < 0)
        {
            Debug.Log("Left Wall Set");
            transform.position = new Vector3(-XPosition, 0, 0);

            //transform.localPosition.Set(-XPosition, 0, 0);
        }   // If Wall Is On The Right
        else if (transform.position.x > 0)
        {
            Debug.Log("Right Wall Set");
            transform.position = new Vector3(XPosition, 0, 0);
            //transform.position.Set(XPosition, 0, 0);
        }

        transform.localScale = new Vector3(1, ((Main_Camera.orthographicSize + 2.625f) /5), 1);

        Debug.Log(XPosition);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
