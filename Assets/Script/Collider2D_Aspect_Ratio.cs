﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collider2D_Aspect_Ratio : MonoBehaviour
{
    private Camera Main_Camera;
    private BoxCollider2D m_Collider;

    // Start is called before the first frame update
    void Start()
    {
        float fColliderSizex;
        Main_Camera = GameObject.Find("Main Camera").GetComponent<Camera>();
        m_Collider = GetComponent<BoxCollider2D>();

        if (transform.position.x == 0)
        {
            fColliderSizex = Main_Camera.orthographicSize * Main_Camera.aspect * 2;
            m_Collider.size = new Vector2(fColliderSizex, m_Collider.size.y);
        }

        if (transform.position.y == 0)
        {
            fColliderSizex = Main_Camera.orthographicSize * Main_Camera.aspect;
            m_Collider.size = new Vector2(m_Collider.size.x, fColliderSizex);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
