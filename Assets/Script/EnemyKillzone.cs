﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyKillzone : MonoBehaviour
{
    private GameObject Player;
    private Level_Manager LvlManager;
    private Camera Main_Camera;

    private float Cooldown = 0.75f;
    private float Timer;
    private bool Death;

    // Start is called before the first frame update
    void Start()
    {
        LvlManager = GameObject.Find("GameManager").GetComponent<Level_Manager>();
        Main_Camera = GameObject.Find("Main Camera").GetComponent<Camera>();
        Player = GameObject.Find("CharacterRobotBoy");

        //Physics2D.IgnoreCollision(Player.GetComponent<BoxCollider2D>(), GetComponent<BoxCollider2D>());
        //Physics2D.IgnoreCollision(Player.GetComponent<CircleCollider2D>(), GetComponent<BoxCollider2D>());
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Collision")
        {
            LvlManager.EnemyDied();
            Death = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
        if(transform.position.y <= (Main_Camera.transform.position.y - Main_Camera.orthographicSize - 2))
        {
            Destroy(gameObject);
        }

        if (Death == true)
        {
            Timer += Time.deltaTime;
        }

        if (Timer > Cooldown)
        {
            Timer = 0;
            Destroy(gameObject);
        }
    }
}
