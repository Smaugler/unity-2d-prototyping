﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingEnemySpawner : MonoBehaviour
{
    public GameObject FlyingEnemy;
    private Level_Manager LvlManager;
    public float FlyingEnemySpawnRate;
    private float SpawnCooldown;
    private float SpawnTimer;

    // Start is called before the first frame update
    void Start()
    {
        LvlManager = GameObject.Find("GameManager").GetComponent<Level_Manager>();
        SpawnCooldown = Random.Range(10, 20);
    }

    // Update is called once per frame
    void Update()
    {
        SpawnTimer += Time.deltaTime;

        if(SpawnTimer >= SpawnCooldown && LvlManager.GameStart == true && LvlManager.Jumps >= 50 && LvlManager.Attacks >= 25)
        {
            SpawnFlyingEnemy();
            SpawnCooldown = Random.Range(10, 20);
            SpawnTimer = 0;
        }
    }

    public void SpawnFlyingEnemy()
    {
        Instantiate(FlyingEnemy, new Vector3(Random.Range(transform.position.x - (GetComponent<BoxCollider2D>().size.x/2) + (FlyingEnemy.GetComponent<BoxCollider2D>().size.x/2), transform.position.x + (GetComponent<BoxCollider2D>().size.x / 2) - (FlyingEnemy.GetComponent<BoxCollider2D>().size.x / 2)), transform.position.y, 0), Quaternion.identity);
    }
}
