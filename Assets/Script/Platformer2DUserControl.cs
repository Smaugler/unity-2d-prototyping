//
// Bachelor of Software Engineering
// Media Design School
// Auckland
// New Zealand
//
// (c) 2019 Media Design School
//
// File Name	: Platformer2DUserControl
// Description	: This script takes the user input and passes them to the PlatformCharacter2D script
// Author		: Alexander Jenkins && Justin Yuen
// Mail			: alexander.jen8470@mediadesign.school.nz
//

using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using System.Collections;

namespace UnityStandardAssets._2D
{
    [RequireComponent(typeof(PlatformerCharacter2D))]
    public class Platformer2DUserControl : MonoBehaviour
    {
        public PlatformerCharacter2D m_Character;
        private Level_Manager LvlManager;

        private bool m_Jump;
        private bool attack;
        private float clickTimer;
        private float delay = 0.5f;
        private bool timered;

        void Start()
        {
            LvlManager = GameObject.Find("GameManager").GetComponent<Level_Manager>();
        }

        private void Awake()
        {
            m_Character = GetComponent<PlatformerCharacter2D>();
            
        }


        private void Update()
        {
          

            if (!m_Jump)
            {
                // Read the jump input in Update so button presses aren't missed.
                m_Jump = CrossPlatformInputManager.GetButtonDown("Jump");
            }
            


        }



        private void FixedUpdate()
        {
            // Read the inputs.

            if (Input.GetKey(KeyCode.J) && timered == false && LvlManager.GameStart == true)
            {
                attack = true;
                timered = true;
            }
            else
            {
                attack = false;
            }

            if (timered)
            {
                clickTimer += Time.deltaTime;
            }

            if(clickTimer >= delay)
            {
                timered = false;
                clickTimer = 0;
            }

            if (LvlManager.GameStart == true)
            {
                float h = CrossPlatformInputManager.GetAxis("Horizontal");

                // Pass all parameters to the character control script.
                m_Character.Move(h / 2, m_Jump, attack);
            }

            m_Jump = false;
            
        }




    }
}
