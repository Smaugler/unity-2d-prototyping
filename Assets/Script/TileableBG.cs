﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileableBG : MonoBehaviour
{
    public GameObject BackgroundTile;
    public GameObject Player;

    private Vector2 InitPosition;
    private BoxCollider2D ImageCollider;
    private Camera Main_Camera;
    private bool tileable = false;

    // Start is called before the first frame update
    void Start()
    {
        tileable = false;
        Player = GameObject.Find("CharacterRobotBoy");
        Main_Camera = GameObject.Find("Main Camera").GetComponent<Camera>();
        ImageCollider = GetComponent<BoxCollider2D>();
        InitPosition.Set(transform.position.x, transform.position.y);
    }

    // Update is called once per frame
    void Update()
    {
        if (Main_Camera.transform.position.y >= (transform.position.y + (ImageCollider.size.y / 2) - (Main_Camera.orthographicSize)) && tileable == false)
        {
            tileable = true;
            Instantiate(BackgroundTile, new Vector3(transform.position.x, (transform.position.y + (ImageCollider.size.y)), 0), Quaternion.identity);
        }

        if (Main_Camera.transform.position.y >= (transform.position.y + ImageCollider.size.y))
        {
            Destroy(gameObject);
        }

        //Debug.Log(Main_Camera.orthographicSize);
    }
}
