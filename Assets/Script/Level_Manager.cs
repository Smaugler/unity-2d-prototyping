﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level_Manager : MonoBehaviour
{
    // Tutorial Milestones
    public bool bTutorialComplete;
    public int WallJumpTask;
    public int JumpTask;
    public int AttackTask;

    public int WallJumps;
    public int Attacks;
    public int Jumps;

    public GameObject Player;
    public GameObject BottomPlatform;
    public int EnemyCount;

    public bool GameStart;

    // Start is called before the first frame update
    void Start()
    {
        GameStart = false;

        if (bTutorialComplete == true)
        {
            WallJumps = WallJumpTask;

            Attacks = AttackTask;

            Jumps = JumpTask;
        }
        else
        {
            WallJumps = 0;

            Attacks = 0;

            Jumps = 0;
        }
        
        EnemyCount = 0;
        //BottomPlatform = GameObject.Find("BottomPlatform");
        //Instantiate(Player, new Vector3(BottomPlatform.transform.position.x, BottomPlatform.transform.position.y+1, BottomPlatform.transform.position.z), Quaternion.identity);
    }
    
    void OnValidate()
    {

    }

    public void NewEnemy()
    {
        EnemyCount += 1;
    }

    public void GameBegin()
    {
        GameStart = true;
    }

    public void GameEnd()
    {
        GameStart = false;
    }

    public void EnemyDied()
    {
        EnemyCount -= 1;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
