using System;
using UnityEngine;
using UnityEngine.SceneManagement;

    public class Restarter : MonoBehaviour
    {

        private Camera Main_Camera;
        private Level_Manager LvlManager;
        private BoxCollider2D PlatformCollider2D;
        private BoxCollider2D PlayerBoxCollider2D;
        private CircleCollider2D PlayerCircleCollider2D;
        private float Cooldown = 1.0f;
        private float Timer;
        public bool Death;

        void Start()
        {
            Main_Camera = GameObject.Find("Main Camera").GetComponent<Camera>();
            PlatformCollider2D = GetComponent<BoxCollider2D>();
            PlayerBoxCollider2D = GameObject.Find("CharacterRobotBoy").GetComponent<BoxCollider2D>();
            PlayerCircleCollider2D = GameObject.Find("CharacterRobotBoy").GetComponent<CircleCollider2D>();
            LvlManager = GameObject.Find("GameManager").GetComponent<Level_Manager>();
            Death = false;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.tag == "Player")
            {
                LvlManager.GameEnd();
                ScoreScript.scoreValue = 0;
                Death = true;
            }
        }

        private void Update()
        {
            if (Death == true)
            {
                Timer += Time.deltaTime;
            }

            if(Timer > Cooldown)
            {
                Timer = 0;
                SceneManager.LoadScene(SceneManager.GetSceneAt(0).name);
            }
        }
    }