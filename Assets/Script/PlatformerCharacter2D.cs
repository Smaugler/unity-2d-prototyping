//
// Bachelor of Software Engineering
// Media Design School
// Auckland
// New Zealand
//
// (c) 2019 Media Design School
//
// File Name	: PlatformerCharacter2D.cs
// Description	: This script implements the character movement controls and attack. It also links to the level manager for scoring and difficulty increases.
// Author		: Alexander Jenkins && Justin Yuen
// Mail			: alexander.jen8470@mediadesign.school.nz
//
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityStandardAssets._2D
{
    public class PlatformerCharacter2D : EnemyKill
    {
        [SerializeField] public float m_MaxSpeed = 10f;                    // The fastest the player can travel in the x axis.
        [SerializeField] public float m_JumpForce = 400f;                  // Amount of force added when the player jumps
        [SerializeField] private readonly bool m_AirControl = true;                 // Whether or not a player can steer while jumping;
        [SerializeField] private LayerMask m_WhatIsGround;                  // A mask determining what is ground to the character
        [SerializeField] private LayerMask m_WhatIsWall;


        public Transform m_GroundCheck;    // A position marking where to check if the player is grounded.
        const float k_GroundedRadius = 0.2f; // Radius of the overlap circle to determine if grounded
        public bool m_Grounded;            // Whether or not the player is grounded.
   
        const float k_CeilingRadius = .01f; // Radius of the overlap circle to determine if the player can stand up
        private Animator m_Anim;            // Reference to the player's animator component.
        private Rigidbody2D m_Rigidbody2D;
        public bool m_FacingRight = true;  // For determining which way the player is currently facing.
       

        private Transform m_WallCheck;    // A position marking where to check if the player is wall jumping.
        const float k_WallRadius = .01f; // Radius of the overlap circle to determine if on a wall
        private bool m_Wall;
        private bool hasJumped;
        private readonly float Tseconds;
        public bool isAttacking;
        private readonly float timeBtwAttack;
        public float startTimeBtwAttack;
        public Transform attackPos;
        public LayerMask whatIsEnemy;
        public float attackRange;
        private bool hasAttacked;
     

        private bool enemyKilled;
        public float EnemyForceAngle;
        public float EnemyForceStrength;
        float lastAttacked = -9999;

        private float AttackTimer;
        private float AttackDelay = 0.25f;
       

        //public float enemyXForce = 500;
        //public float enemyYForce = 100;

        private Level_Manager LvlManager;

        public float WallClimbDescentSpeed;

        public float walljumpforcex = 200;

        public GameObject StartPlatform;
        public Camera2D Main_Camera;
        public BlockSpawner Spawner;
        public GirlFrog GirlFrog;

        private void Start()
        {
            StartPlatform = GameObject.Find("StartPlatform");
            LvlManager = GameObject.Find("GameManager").GetComponent<Level_Manager>();
            Main_Camera = GameObject.Find("Main Camera").GetComponent<Camera2D>();
            Spawner = GameObject.Find("Platform Spawner").GetComponent<BlockSpawner>();
            GirlFrog = GameObject.Find("GirlFrog").GetComponent<GirlFrog>();
            WallClimbDescentSpeed = -4;

            transform.position.Set(StartPlatform.transform.position.x, StartPlatform.transform.position.y + 1, StartPlatform.transform.position.z);
        }

        private void OnValidate()
        {
            attackRange = Mathf.Clamp(attackRange, 0.0f, 3.0f);
            EnemyForceAngle = Mathf.Clamp(EnemyForceAngle, 0.0f, 90.0f);
            EnemyForceStrength = Mathf.Clamp(EnemyForceStrength, 1.0f, 10.0f);
        }

        private void Awake()
        {
            // Setting up references.
            m_GroundCheck = transform.Find("GroundCheck");
         
            m_WallCheck = transform.Find("WallCheck");
            m_Anim = GetComponent<Animator>();
            m_Rigidbody2D = GetComponent<Rigidbody2D>();
            hasAttacked = false;
            

        }
    
        //Fixed Updates
        private void FixedUpdate()
        {
            m_Grounded = false;
            m_Wall = false;
            


            //Reset jump
            if (m_Rigidbody2D.velocity.y >= 15)
            {
                m_Rigidbody2D.gravityScale += 2;
            } else if (!m_Grounded && m_Rigidbody2D.velocity.y < 0)
                {
                    m_Rigidbody2D.gravityScale = 5;
                }

            // The player is grounded if a circlecast to the groundcheck position hits anything designated as ground
            // This can be done using layers instead but Sample Assets will not overwrite your project settings.

            Collider2D[] colliders = Physics2D.OverlapCircleAll(m_GroundCheck.position, k_GroundedRadius, m_WhatIsGround);
            for (int i = 0; i < colliders.Length; i++)
            {
                if (m_Rigidbody2D.velocity.y <= 0)
                {
                    m_Grounded = true;
                }
                hasJumped = false;
                m_Anim.SetBool("Walling", false);
            }
            m_Anim.SetBool("Ground", m_Grounded);

            // Set the vertical animation
            m_Anim.SetFloat("vSpeed", m_Rigidbody2D.velocity.y);

            // The player is Wall Jumping if a circlecast to the wallcheck position hits anything designated as wall
          
            Collider2D[] Wallcolliders = Physics2D.OverlapCircleAll(m_WallCheck.position, k_WallRadius, m_WhatIsWall);
            for (int i = 0; i < Wallcolliders.Length; i++)
            {
                if (Wallcolliders[i].gameObject != gameObject)
                    m_Wall = true;
                hasJumped = false;

            }

            //Reset bool after attacking
            if (!isAttacking && m_Anim.GetBool("attack"))
            {

                    m_Anim.SetBool("attack", false);

            }
            else if (!isAttacking && m_Anim.GetBool("jumpAttack"))
            {

                    m_Anim.SetBool("jumpAttack", false);
 
            }

            if (hasAttacked)
            {
                Attack();
                hasAttacked = false;
            }
        }

       
        //Movement Scripts
        public void Move(float move, bool jump, bool attack)
        {
           

            if (attack && !m_Anim.GetCurrentAnimatorStateInfo(0).IsName("Attack"))
            {
                isAttacking = true;
            }
            
            //Attck script
            if (isAttacking)
            {
                AttackTimer += Time.deltaTime;

                if (m_Grounded)
                {
                    m_Rigidbody2D.velocity = new Vector2(0, m_Rigidbody2D.velocity.y);
                    m_Anim.SetBool("attack", attack);
                }
                else if (!m_Grounded && !m_Wall)
                {
                    m_Anim.SetBool("jumpAttack", attack);
                }

                if(AttackTimer >= AttackDelay)
                {
                    isAttacking = false;
                    hasAttacked = true;
                    AttackTimer = 0;
                }
            }

          
            
           

            //Check wall colision and set walljumping to true
            if (Physics2D.OverlapCircle(m_WallCheck.position, k_WallRadius, m_WhatIsWall))
            {
                m_Wall = true;

            } else
            {
                m_Wall = false;
            }

            //only control the player if grounded or airControl is turned on
            if (m_Grounded || m_AirControl && hasJumped == false && !m_Anim.GetBool("attack"))
            {
               

                // The Speed animator parameter is set to the absolute value of the horizontal input.
                m_Anim.SetFloat("Speed", Mathf.Abs(move));


                // Move the character
                m_Rigidbody2D.velocity = new Vector2(move * m_MaxSpeed, m_Rigidbody2D.velocity.y);



                // If the input is moving the player right and the player is facing left...
                if (move > 0 && !m_FacingRight && m_Wall == false)
                {
                    // ... flip the player.
                    Flip();
                }
                // Otherwise if the input is moving the player left and the player is facing right...
                else if (move < 0 && m_FacingRight && m_Wall == false)
                {
                    // ... flip the player.
                    Flip();
                }
            }
            else if (m_AirControl && hasJumped == true && move == 0 && !m_Wall)
            {
                m_Anim.SetFloat("Speed", Mathf.Abs(move));
                m_Rigidbody2D.velocity = new Vector2(m_Rigidbody2D.velocity.x, m_Rigidbody2D.velocity.y);
            }
            else if (m_AirControl && hasJumped == true && move > 0 && m_Rigidbody2D.velocity.x < m_MaxSpeed && !m_Wall)
            {
                m_Anim.SetFloat("Speed", Mathf.Abs(move));
                m_Rigidbody2D.velocity = new Vector2(move * m_MaxSpeed, m_Rigidbody2D.velocity.y );
            }
            else if (m_AirControl && hasJumped == true && move < 0 && Mathf.Abs(m_Rigidbody2D.velocity.x) < m_MaxSpeed && !m_Wall)
            {
                m_Anim.SetFloat("Speed", Mathf.Abs(move));
                m_Rigidbody2D.velocity = new Vector2(move * m_MaxSpeed, m_Rigidbody2D.velocity.y);
            }

            // If the player should jump...
            if (m_Grounded && jump && m_Anim.GetBool("Ground") && !m_Wall)
            {
                // Add a vertical force to the player.
               
                m_Grounded = false;
                m_Anim.SetBool("Ground", false);
                m_Rigidbody2D.gravityScale = 0;
                m_Rigidbody2D.AddForce(new Vector2(0f, m_JumpForce));
                LvlManager.Jumps += 1;
            }
            
            //Slide slowly down on the wall
            if (m_Grounded == false && m_Wall == true && m_Rigidbody2D.velocity.y < 0)
            {

                // Add a vertical force to the player.
                m_Anim.SetBool("Walling", m_Wall);
                m_Rigidbody2D.gravityScale = 0;
                
                m_Rigidbody2D.velocity = new Vector2(0, 0);
                m_Rigidbody2D.velocity = new Vector2(0, WallClimbDescentSpeed);

            }

                //wall jump
            if (m_Grounded == false && jump && m_Wall == true)
            {
                hasJumped = true;
                if (m_FacingRight)
                {
                    // Add a vertical force to the player.
                    m_Wall = false;
                    m_Anim.SetBool("Walling", m_Wall);
                    m_Rigidbody2D.velocity = new Vector2(0, 0);
                    m_Rigidbody2D.AddForce(new Vector2(-walljumpforcex, m_JumpForce));
                    Flip();
                }
                else if (!m_FacingRight)
                {
                    // Add a vertical force to the player.
                    m_Wall = false;
                    m_Anim.SetBool("Walling", m_Wall);
                    m_Rigidbody2D.velocity = new Vector2(0, 0);
                    m_Rigidbody2D.AddForce(new Vector2(walljumpforcex, m_JumpForce));
                    Flip();
                }

                if (LvlManager.Jumps > LvlManager.JumpTask)
                {
                    LvlManager.WallJumps += 1;
                }
            }  
        }

        //Flip character direction
        public void Flip()
        {
            // Switch the way the player is labelled as facing.
            m_FacingRight = !m_FacingRight;

            // Multiply the player's x local scale by -1.
            Vector3 theScale = transform.localScale;
            theScale.x *= -1;
            transform.localScale = theScale;
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(attackPos.position, attackRange);
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(m_GroundCheck.position, k_GroundedRadius);
        }


        private void Attack()
        {
            //Check for enemies in range
            Collider2D[] enemyKill = Physics2D.OverlapCircleAll(attackPos.position, attackRange, whatIsEnemy);

            for (int i = 0; i < enemyKill.Length; i++)
            {
                if (enemyKill[i].gameObject != gameObject)
                {
                    //Set force angles based on enemy position from player
                    float fAngle = EnemyForceAngle / 180;
                    float fForceDirection = 1.0f;
                    if (enemyKill[i].gameObject.GetComponent<ToadAI>().movingRight)
                    {
                        enemyKill[i].gameObject.GetComponent<ToadAI>().flip();
                    }
                    
                    if (!m_FacingRight)
                    {
                        fForceDirection = -1.0f;
                        if (!enemyKill[i].gameObject.GetComponent<ToadAI>().movingRight)
                        {
                            enemyKill[i].gameObject.GetComponent<ToadAI>().flip();
                        }
                    }

                    GirlFrog.isCheer = true;
                    ScoreScript.scoreValue += 100;
                    //Send enemy flying
                    enemyKill[i].gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(Mathf.Cos(fAngle * Mathf.PI) * EnemyForceStrength * 100 * fForceDirection, Mathf.Sin(fAngle * Mathf.PI) * EnemyForceStrength * 100));
                    enemyKill[i].gameObject.GetComponent<ToadAI>().isHit = true;
                    enemyKill[i].gameObject.GetComponent<Animator>().SetTrigger("death");
                    LvlManager.Attacks += 1;
                    Spawner.NormalEnemySpawnRate += 0.05f;

                    if (Main_Camera.Speed < 4.0f)
                    {
                        Main_Camera.Speed += 0.025f;
                        
                    }

                    if(Spawner.NormalEnemySpawnRate < 1)
                    {
                        Spawner.NormalEnemySpawnRate += 0.05f;
                    }

                    Destroy(enemyKill[i].gameObject.GetComponent<BoxCollider2D>());
                }
            }
        }
    }
}
