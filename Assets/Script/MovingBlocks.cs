﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingBlocks : MonoBehaviour
{
    public float fSpeed;

    private Vector2 InitPos;
    private Vector2 Target;
    private GameObject Player;
    private CircleCollider2D PlayerCircleCollider2D;
    private BoxCollider2D PlayerBoxCollider2D;
    private BoxCollider2D m_Collider;
    private Camera Main_Camera;
    private BlockSpawner Spw;

    // Start is called before the first frame update
    void Start()
    {
        Player = GameObject.Find("CharacterRobotBoy");
        PlayerBoxCollider2D = Player.GetComponent<BoxCollider2D>();
        PlayerCircleCollider2D = Player.GetComponent<CircleCollider2D>();

        // Get Objects Collider
        m_Collider = GameObject.Find("Platform Spawner").GetComponent<BoxCollider2D>();

        // Finds Main Camera Object and Assigns it to public variable
        Main_Camera = GameObject.Find("Main Camera").GetComponent<Camera>();

        Spw = GameObject.Find("Platform Spawner").GetComponent<BlockSpawner>();
        
    }

    // Update is called once per frame
    void Update()
    {
        float PlatformInterval = Spw.PlatformInterval;

        float fCameraBottom = Main_Camera.transform.position.y - (Main_Camera.orthographicSize) + (m_Collider.size.y/2 + 1.5f) - PlatformInterval;

        // if object is off screen spawn another block before deleting itself
        if (transform.position.y <= fCameraBottom)
        {
            if (Spw != null)
            {
                Spw.Spawn();
            }

            Destroy(gameObject);
        }
    }
}
