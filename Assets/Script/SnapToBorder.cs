﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnapToBorder : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        float PositionX;
        float PositionY;

        // Finds Main Camera Object and Assigns it to public variable
        Camera Main_Camera = GameObject.Find("Main Camera").GetComponent<Camera>();
        BoxCollider2D m_Collider = GetComponent<BoxCollider2D>();

        PositionX = (Main_Camera.orthographicSize * Main_Camera.aspect);
        PositionY = (Main_Camera.orthographicSize);

        // If Object Is On The Left Or Right
        if (transform.position.x < 0)
        {
            transform.position = new Vector3(-PositionX, 0, 0);

            transform.localScale = new Vector3(1, ((Main_Camera.orthographicSize + 2.625f) / 5), 1);
        }
        else if (transform.position.x > 0)
        {
            transform.position = new Vector3(PositionX, 0, 0);

            transform.localScale = new Vector3(1, ((Main_Camera.orthographicSize + 2.625f) / 5), 1);
        }

        // If Object Is On The Up Or Down
        if (transform.position.y < 0)
        {
            transform.position = new Vector3(0, -PositionY, 0);

            transform.localScale = new Vector3((Main_Camera.orthographicSize * Main_Camera.aspect * 2), 1, 1);
        }
        else if (transform.position.y > 0)
        {
            transform.position = new Vector3(0, PositionY, 0);

            transform.localScale = new Vector3((Main_Camera.orthographicSize * Main_Camera.aspect * 2), 1, 1);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
